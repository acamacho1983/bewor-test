<?php

namespace Vocces\Company\Application;

use Vocces\Company\Domain\Company;
use Vocces\Company\Domain\ValueObject\CompanyId;
use Vocces\Company\Domain\ValueObject\CompanyStatus;
use Vocces\Company\Domain\CompanyRepositoryInterface;
use Vocces\Shared\Domain\Interfaces\ServiceInterface;

class CompanyActivator implements ServiceInterface
{
    /**
     * @var CompanyRepositoryInterface $repository
     */
    private $repository;

    /**
     * Create new instance
     */
    public function __construct(CompanyRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Create a new company
     */
    public function handle($id, $status)
    {
        $responseCompany = $this->repository->findCompanyById(
            $id
        );                        

        if ($status == 'active' && $responseCompany->status == 'inactive') { 
            $this->repository->updateStatus($id, $status);
        }

        return $responseCompany;
    }
}
