<?php

namespace Vocces\Company\Infrastructure;

use App\Models\Company as ModelsCompany;
use Vocces\Company\Domain\Company;
use Vocces\Company\Domain\CompanyRepositoryInterface;

class CompanyRepositoryEloquent implements CompanyRepositoryInterface
{
    /**
     * @inheritDoc
     */
    public function create(Company $company): void
    {
        ModelsCompany::create([
            'id'     => $company->id(),
            'name'   => $company->name(),
            'status' => $company->status(),
        ]);
    }

    public function updateStatus(string $id, string $status): void
    {
        ModelsCompany::where(['id' => $id])
        ->update([            
            'status' => $status,
        ]);
    }

    public function findCompanyById(string $id)
    {
        return ModelsCompany::where([
            'id' => $id])            
        ->first();
    }

    public function findAllCompany()
    {        
        return ModelsCompany::get();
    }
}
