<?php

namespace Vocces\Client\Domain;

interface ClientRepositoryInterface
{    
    public function findAllClients();
}
