<?php

namespace Vocces\Client\Domain\ValueObject;

final class ClientSecondSurname
{

    private $secondSurname;

    public function __construct(string $secondSurname)
    {
        $this->secondSurname = $secondSurname;
    }

    public function get(): string
    {
        return $this->secondSurname;
    }

    public function __toString()
    {
        return $this->secondSurname;
    }
}
