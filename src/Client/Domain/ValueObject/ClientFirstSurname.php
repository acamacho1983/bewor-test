<?php

namespace Vocces\Client\Domain\ValueObject;

final class ClientFirstSurname
{

    private $firstSurname;

    public function __construct(string $firstSurname)
    {
        $this->firstSurname = $firstSurname;
    }

    public function get(): string
    {
        return $this->firstSurname;
    }

    public function __toString()
    {
        return $this->firstSurname;
    }
}
