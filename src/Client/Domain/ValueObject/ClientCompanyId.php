<?php

namespace Vocces\Client\Domain\ValueObject;

final class ClientCompanyId
{

    private $companyId;

    public function __construct(string $companyId)
    {
        $this->companyId = $companyId;
    }

    public function get(): string
    {
        return $this->companyId;
    }

    public function __toString()
    {
        return $this->companyId;
    }
}
