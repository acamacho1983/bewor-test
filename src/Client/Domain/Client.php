<?php

namespace Vocces\Client\Domain;

use Vocces\Client\Domain\ValueObject\ClientId;
use Vocces\Client\Domain\ValueObject\ClientName;
use Vocces\Client\Domain\ValueObject\ClientFirstSurname;
use Vocces\Client\Domain\ValueObject\ClientSecondtSurname;
use Vocces\Client\Domain\ValueObject\ClientCompanyId;
use Vocces\Shared\Infrastructure\Interfaces\Arrayable;

final class Client implements Arrayable
{
    /**
     * @var \Vocces\Client\Domain\ValueObject\ClientId
     */
    private $id;

    /**
     * @var \Vocces\Client\Domain\ValueObject\ClientName
     */
    private $name;

    /**
     * @var \Vocces\Client\Domain\ValueObject\ClientFirstSurname
     */
    private $firstSurname;

    /**
     * @var \Vocces\Client\Domain\ValueObject\ClientSecondSurname
     */
    private $secondSurname;

    /**
     * @var \Vocces\Client\Domain\ValueObject\ClientCompanyId
     */
    private $companyId;

    public function __construct(
        ClientId $id,
        ClientName $name,
        ClientFirstSurname $firstSurname,
        ClientSecondSurname $secondSurname,
        ClientCompanyId $companyId
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->firstSurname = $firstSurname;
        $this->secondSurname = $secondSurname;
        $this->companyId = $companyId;
    }

    public function id(): ClientId
    {
        return $this->id;
    }

    public function name(): ClientName
    {
        return $this->name;
    }

    public function firstSurname(): ClientFirstSurname
    {
        return $this->firstSurname;
    }

    public function secondSurname(): ClientSecondSurname
    {
        return $this->secondSurname;
    }

    public function companyId(): ClientCompanyId
    {
        return $this->companyId;
    }

    public function toArray()
    {
        return [
            'id'                => $this->id()->get(),
            'name'              => $this->name()->get(),
            'first_surname'     => $this->firstSurname()->get(),
            'second_surname'    => $this->secondSurname()->get(),
            'company_id'        => $this->companyId()->get()
        ];
    }
}
