<?php

namespace Vocces\Client\Infrastructure;

use App\Models\Client as ModelsClient;
use Vocces\Client\Domain\Client;
use Vocces\Client\Domain\ClientRepositoryInterface;

class ClientRepositoryEloquent implements ClientRepositoryInterface
{    
    public function findAllClients()
    { 
        return ModelsClient::get();
    }    
}
