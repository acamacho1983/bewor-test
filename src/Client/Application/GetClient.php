<?php

namespace Vocces\Client\Application;

use Vocces\Client\Domain\Client;
use Vocces\Client\Domain\ClientRepositoryInterface;
use Vocces\Shared\Domain\Interfaces\ServiceInterface;

class GetClient implements ServiceInterface
{
    /**
     * @var ClientRepositoryInterface $repository
     */
    private $repository;

    /**
     * Create new instance
     */
    public function __construct(ClientRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * List all clients
     */
    public function handle()
    { 
        return $this->repository->findAllClients();
    }
}
