<?php

namespace Test\Vocces\Company\Application;

use Tests\TestCase;
use Illuminate\Support\Str;
use Vocces\Company\Domain\Company;
use Vocces\Company\Application\CompanyActivator;
use Tests\Vocces\Company\Infrastructure\CompanyRepositoryFake;

final class UpdateStatusCompanyTest extends TestCase
{
    /**
     * @group application
     * @group company
     * @test
     */
    public function updateStatusCompany()
    {
        /**
         * Preparing
         */        
        $testCompany = [
            'id'     => '1',
            'status' => 'active',
        ];

        /**
         * Actions
         */
        $activator = new CompanyActivator(new CompanyRepositoryFake());
        $company = $activator->handle(
            $testCompany['id'],
            $testCompany['status']
        );

        /**
         * Assert
         */
        $this->assertInstanceOf(Company::class, $company);
        $this->assertEquals($testCompany, $company->toArray());
    }
}
