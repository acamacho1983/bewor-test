<?php

namespace Tests\Vocces\Company\Infrastructure;

use Vocces\Company\Domain\Company;
use Vocces\Company\Domain\CompanyRepositoryInterface;

class CompanyRepositoryFake implements CompanyRepositoryInterface
{
    public $callMethodCreate = false;

    /**
     * @inheritdoc
     */
    public function create(Company $company): void
    {
        $this->callMethodCreate = true;
    }

    public function updateStatus( $id, $status): void
    {
        
    }

    public function findCompanyById($id)
    {

    }

    public function findAllCompany()
    {

    }
}
