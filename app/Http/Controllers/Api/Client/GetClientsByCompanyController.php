<?php

namespace App\Http\Controllers\Api\Client;

use App\Http\Controllers\Controller;
use Vocces\Client\Application\GetClient;

class GetClientsByCompanyController extends Controller
{    

    /**
     * Get list clients.
     *     
     * @return \Illuminate\Http\Response
     */
    public function __invoke(GetClient $service)
    {        
        try {
            $client = $service->handle();
            return response($client, 201);
        } catch (\Throwable $error) {
            throw $error;
        }
    }    
}
