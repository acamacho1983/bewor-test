<?php

namespace App\Http\Controllers\Api\Company;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Http\Requests\Company\UpdateCompanyStatusRequest;
use Vocces\Company\Domain\ValueObject\CompanyStatus;
use Vocces\Company\Application\CompanyActivator;

class UpdateCompanyStatusController extends Controller
{
    

    /**
     * Update status company.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  company  $id
     * @return \Illuminate\Http\Response
     */
    public function __invoke(UpdateCompanyStatusRequest $request, Company $company, CompanyActivator $service)
    {        
        DB::beginTransaction();
        try {
            $company = $service->handle($company->id, $request->status); 
            DB::commit();
            return response($company, 201);
        } catch (\Throwable $error) {
            DB::rollback();
            throw $error;
        }
    }
}

