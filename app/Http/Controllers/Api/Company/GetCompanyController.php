<?php

namespace App\Http\Controllers\Api\Company;

use App\Http\Controllers\Controller;
use Vocces\Company\Application\GetCompany;

class GetCompanyController extends Controller
{
    

    /**
     * Get list company.
     *     
     * @return \Illuminate\Http\Response
     */
    public function __invoke(GetCompany $service)
    {        
        try {
            $company = $service->handle();
            return response($company, 201);
        } catch (\Throwable $error) {
            throw $error;
        }        
    }
    
}

